#!/bin/bash -e

INIT_SEM=./initialized.sem

pip install -r requirements.txt

if [ ! -f $INIT_SEM ]; then
    django-admin startproject tablonOnline .
    touch $INIT_SEM
fi

while ! nc -z db 5432; do
    sleep 5
done

python3 manage.py migrate
python3 manage.py runserver 0.0.0.0:8000
