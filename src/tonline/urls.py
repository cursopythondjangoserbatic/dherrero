from django.urls import path
from django.conf.urls import include, url
from . import views

app_name = 'tonline'

urlpatterns = [
    # path('', views.Index.as_view(), name='index'),
    path('', views.index, name='index'),
    path('ciudad/<int:ciudad>', views.ListaCiudad.as_view(), name='listaCiudad'),
    path('lc/<int:categoria>', views.ListaCategoria.as_view(), name='lista'),
    path('lu/', views.ListaUsiario.as_view(), name='listausr'),
    path('d/<int:pk>', views.DetalleT.as_view(), name='detalle'),  # Por Id
    path('c/', views.CrearT.as_view(), name='crear'),
    path('u/<int:pk>', views.actualizarT.as_view(), name='actualizar'),
    path('b/<int:pk>', views.BorrarT.as_view(), name='borrar'),
    path('<slug>', views.DetalleT.as_view(), name='detallee'),  # por Slug
    # path('signup/', views.signup, name='signup'),
    # url(r'^(?P<slug>[-_\w]+)/$', views.DetalleT.as_view(), name='detallee'),#por Slug
]
