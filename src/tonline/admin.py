from django.contrib import admin
from .models import Categoria, Scategoria, Ciudad, Articulo, File


class CategoriaInline(admin.StackedInline):
    model = Categoria


class ScategoriaInline(admin.StackedInline):
    list_display = ('nombre', 'categoria')
    model = Scategoria


class CiudadAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    model = Ciudad


class FileInline(admin.StackedInline):
    model = File


class ArticuloAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'fecha', 'ciudad', 'precio', 'visitas')
    model = Articulo
    inlines = [
        FileInline,
    ]


admin.site.register(Ciudad, CiudadAdmin)
admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Scategoria)
admin.site.register(Categoria)
admin.site.register(File)
