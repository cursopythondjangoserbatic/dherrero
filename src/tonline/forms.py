from django import forms
from django.forms import inlineformset_factory

from .models import Articulo, File

"""
class ArticuloForm (forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ["nombre", "descripcion", "categoria", "tipo", "ciudad", "precio", "imagen"]
        widgets = {
        "descripcion": forms.Textarea(),
        }
        
class FileForm(forms.ModelForm):
    class Meta:
        model=File
        fields = ["articulo", "imagenes"]
"""


class ArticuloForm(forms.ModelForm):
    class Meta:
        model = Articulo
        exclude = ("slug", "creado", "visitas")


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        exclude = ()


FileInlineFormSet = inlineformset_factory(Articulo, File,
                                          form=FileForm, extra=1)
"""
FileFormSet = forms.modelformset_factory(
    File,
    form = FileForm,
    extra = 1,
)

FileInlineFormSet = inlineformset_factory(
    Articulo,
    File,
    form = FileForm,
    formset = FileFormSet,
    extra = 1
)
"""
