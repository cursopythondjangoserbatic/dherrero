from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .forms import FileInlineFormSet
from .models import Articulo, Scategoria, Ciudad

"""
class Index(ListView):
    model=Scategoria
    template_name = "index.html"
"""
def index (request):
    template = "index.html"
    queryset1 = Scategoria.objects.all()
    queryset2 = Ciudad.objects.all()
    context = {
        "ciudades" : queryset2,
        "scategoria_list" : queryset1,
    }
    return render(request, template, context)

class ListaCiudad(ListView):
    model = Articulo
    template_name = "tonline/articulo_list.html"
    
    def get_queryset(self):
        return Articulo.sr.srCiudad(self.kwargs['ciudad'])
        
class ListaCategoria(ListView):
    model = Articulo
    template_name = "tonline/articulo_list.html"
    
    def get_queryset(self):
        return Articulo.sr.srCategoria(self.kwargs['categoria'])

class ListaUsiario(LoginRequiredMixin, ListView):
    model = Articulo
    template_name = "tonline/articulo_list.html"
    
    def get_queryset(self):
        return Articulo.sr.srUser(self.request.user)

class ListaUsiario(LoginRequiredMixin, ListView):
    model = Articulo
    template_name = "tonline/articulo_list.html"
    
    def get_queryset(self):
        return Articulo.sr.srCiudad(self.kwargs['categoria'])


"""
class CrearT(LoginRequiredMixin, CreateView): 
    #model = Articulo
    form_class = ArticuloForm
    #fields = ['nombre', 'descripcion', 'tipo', 'ciudad', 'precio']
    #form = ArticuloForm
    template_name = "tonline/articulo_form.html"
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creado = self.request.user
        self.object.save()
        return super(CrearT, self).form_valid(form)
"""
class CrearT(LoginRequiredMixin, CreateView):
    model = Articulo
    fields = ["nombre", "descripcion", "categoria", "tipo", "ciudad", "precio"]
    success_url = reverse_lazy('tonline:listausr')

    def get_context_data(self, **kwargs):
        data = super(CrearT, self).get_context_data(**kwargs)
        print(self.request.POST)
        if self.request.POST:
            data['file'] = FileInlineFormSet(self.request.POST, self.request.FILES)
        else:
            data['file'] = FileInlineFormSet()
        return data

    def form_valid(self, form):

        context = self.get_context_data()
        file = context['file']
        with transaction.atomic():
            self.object = form.save()
            self.object.creado = self.request.user
            if file.is_valid():
                file.instance = self.object
                file.save()
        return super(CrearT, self).form_valid(form)

class BorrarT(LoginRequiredMixin, DeleteView): 
    model = Articulo
    
    def get(self, request, *args, **kwargs):
        Articulo1 = Articulo.sr.get(id=self.kwargs['pk'])     # Por id -->(id=self.kwargs['pk'])
        if Articulo1.creado.id == self.request.user.id:
            return super().get(request, *args, **kwargs)
        else:
            return redirect('/')
            
    def get_success_url(self):
        return reverse("listausr")
"""
class actualizarT(LoginRequiredMixin, UpdateView):
    model = Articulo
    form_class = ArticuloForm
    template_name = "tonline/articulo_form.html"
    
    def get(self, request, *args, **kwargs):
        Articulo1 = Articulo.sr.get(id=self.kwargs['pk'])     # Por id -->(id=self.kwargs['pk'])
        if Articulo1.creado.id == self.request.user.id:
            return super().get(request, *args, **kwargs)
        else:
            return redirect('/')
"""
class actualizarT(LoginRequiredMixin, UpdateView):
    model = Articulo
    fields = ["nombre", "descripcion", "categoria", "tipo", "ciudad", "precio"]
    success_url = reverse_lazy('tonline:listausr')

    def get_context_data(self, **kwargs):
        data = super(actualizarT, self).get_context_data(**kwargs)
        if self.request.POST:
            data['file'] = FileInlineFormSet(self.request.POST, self.request.FILES, instance=self.object)
        else:
            data['file'] = FileInlineFormSet(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        file = context['file']
        with transaction.atomic():
            self.object = form.save()

            if file.is_valid():
                file.instance = self.object
                file.save()
        return super(actualizarT, self).form_valid(form)
        
    def get(self, request, *args, **kwargs):
        Articulo1 = Articulo.sr.get(id=self.kwargs['pk'])     # Por id -->(id=self.kwargs['pk'])
        if Articulo1.creado.id == self.request.user.id:
            return super().get(request, *args, **kwargs)
        else:
            return redirect('/')


class DetalleT(DetailView):
    model = Articulo

    #Contador de visitas por articulo

    def get(self, request, *args, **kwargs):
        Articulo1 = Articulo.sr.get(slug=self.kwargs['slug'])     # Por id -->(id=self.kwargs['pk'])
        Articulo1.visitas += 1
        Articulo1.save()
        return super().get(request, *args, **kwargs)
"""
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'tonline/signup.html', {'form': form})
"""