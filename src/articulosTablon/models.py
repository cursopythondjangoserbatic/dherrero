from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from ckeditor.fields import RichTextField

class Categoria(models.Model):
    nombre = models.CharField(max_length=20, verbose_name='Categoria')
    slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return self.nombre


def categoria_pre_save_receptor(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nombre)


pre_save.connect(categoria_pre_save_receptor, sender=Categoria)


class Scategoria(models.Model):
    nombre = models.CharField(max_length=20, verbose_name='Subcategoria')
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, null=False, verbose_name='Categoria')
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name = 'Sub categoria'
        verbose_name_plural = 'Sub categorias'

    def __str__(self):
        return self.nombre


def scategoria_pre_save_receptor(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nombre)


pre_save.connect(scategoria_pre_save_receptor, sender=Scategoria)


class Ciudad(models.Model):
    nombre = models.CharField(max_length=20, verbose_name='Ciudad')
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Ciudades'

    def __str__(self):
        return self.nombre


def ciudad_pre_save_receptor(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nombre)


pre_save.connect(ciudad_pre_save_receptor, sender=Ciudad)


class ArticuloModelManager(models.Manager):
    def srCategoria(self, categoria):
        return super(ArticuloModelManager, self).filter(categoria__slug=categoria)

    def srUser(self, usr):
        return super(ArticuloModelManager, self).filter(creado=usr)

    def srUserArg(self, usr):
        return super(ArticuloModelManager, self).filter(creado=usr)

    def srCiudad(self, ciudad):
        return super(ArticuloModelManager, self).filter(ciudad__slug=ciudad)


class Articulo(models.Model):
    TYPE_ADD = (
        ('pa', 'Particular'),
        ('pr', 'Privado'),
    )

    categoria = models.ForeignKey(Scategoria, on_delete=models.CASCADE, null=False, verbose_name='Categoria')
    nombre = models.CharField(max_length=150, verbose_name='Descripción corta')
    #descripcion = models.TextField(max_length=500, verbose_name='Descripcion')
    descripcion = RichTextField(verbose_name='Descripcion')
    fechaC = models.DateTimeField(auto_now_add=True, verbose_name='Fecha creacion')
    fechaM = models.DateTimeField(auto_now=True, verbose_name='Fecha modificacion')
    tipo = models.CharField(max_length=2, choices=TYPE_ADD, verbose_name='Tipo de vendedor')
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE, null=False, verbose_name='Ciudad')
    precio = models.FloatField(verbose_name='Precio')
    visitas = models.IntegerField(verbose_name='Nº Visitas', default=0)
    visible = models.BooleanField(verbose_name='Publicado', default=True)
    creado = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)

    objects = ArticuloModelManager()

    class Meta:
        ordering = ['-fechaM', '-fechaC']

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return "/ficha/%s" % (self.slug)


def articulo_pre_save_receptor(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.nombre)


pre_save.connect(articulo_pre_save_receptor, sender=Articulo)


class File(models.Model):
    articulo = models.ForeignKey(Articulo, on_delete=models.CASCADE)
    imagenes = models.ImageField(upload_to='static')
