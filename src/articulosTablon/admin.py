from django.contrib import admin
from .models import Categoria, Scategoria, Ciudad, Articulo, File


class ScategoriaInline(admin.StackedInline):
    model = Scategoria


class CategoriaAdmin(admin.ModelAdmin):
    model = Categoria
    inlines = [
        ScategoriaInline,
    ]


class CiudadAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    model = Ciudad


class FileInline(admin.StackedInline):
    model = File

class ArticuloAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'ciudad', 'precio', 'visitas')
    readonly_fields = ('fechaC', 'fechaM')
    ordering = ('fechaM', 'fechaC')
    search_fields = ('nombre','ciudad__nombre','precio', 'categoria__nombre')
    list_filter = ('ciudad__nombre', 'categoria__nombre')
    model = Articulo
    inlines = [
        FileInline,
    ]


admin.site.register(Ciudad, CiudadAdmin)
admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(File)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Scategoria)

