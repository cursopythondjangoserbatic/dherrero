"""tablonOnline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='inicio'),
    path('ciudad/<slug:ciudad>', views.ListaCiudad.as_view(), name='ciudadSr'),
    path('categoria/<slug:categoria>', views.ListaCategoria.as_view(), name='categoriaSr'),
    path('usuario/', views.ListaUsuario.as_view(), name='ususarioSrArg'),
    path('usuario/<int:pk>', views.ListaUsuario.as_view(), name='ususarioSrArg'),
    path('ficha/<slug:slug>', views.DetalleArticulo.as_view(), name='ficha'),
    path('crear/', views.CrearArticulo.as_view(), name='crear'),
    path('actualizar/<int:pk>', views.ActualizarArticulo.as_view(), name='actualizar'),
    path('borrar/<int:pk>', views.BorrarArticulo.as_view(), name='borrar'),
]
