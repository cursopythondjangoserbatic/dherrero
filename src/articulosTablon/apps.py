from django.apps import AppConfig


class ArticulostablonConfig(AppConfig):
    name = 'articulosTablon'
    verbose_name='Anuncios del Tablon'
