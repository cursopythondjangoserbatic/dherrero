from django import forms
from django.forms import inlineformset_factory
from ckeditor.widgets import CKEditorWidget

from .models import Articulo, File


class ArticuloForm(forms.ModelForm):
    class Meta:
        model = Articulo
        descripcion = forms.CharField(widget=CKEditorWidget())
        exclude = ("slug", "fechaC", "visitas", "fechaM")


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        exclude = ()


FileInlineFormSet = inlineformset_factory(Articulo, File,
                                          form=FileForm, extra=1)

