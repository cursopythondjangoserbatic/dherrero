from django.db import transaction
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Ciudad, Scategoria, Articulo
from.forms import ArticuloForm, FileForm, FileInlineFormSet

def index(request):
    template = "index.html"
    queryset1 = Scategoria.objects.all()
    queryset2 = Ciudad.objects.all()
    context = {
        "ciudades": queryset2,
        "scategoria_list": queryset1,
    }
    return render(request, template, context)


class ListaCiudad(ListView):
    model = Articulo
    template_name = "articulosTablon/articulo_list.html"

    def get_queryset(self):
        return Articulo.objects.srCiudad(self.kwargs['ciudad'])


class ListaCategoria(ListView):
    model = Articulo
    template_name = "articulosTablon/articulo_list.html"

    def get_queryset(self):
        return Articulo.objects.srCategoria(self.kwargs['categoria'])


class ListaUsuario (ListView):
    model = Articulo
    template_name = "articulosTablon/articulo_list.html"

    def get_queryset(self):
        if 'pk' in self.kwargs:
            return Articulo.objects.srUserArg(self.kwargs['pk'])
        elif not self.request.user.is_authenticated:
            pass
        else:
            return Articulo.objects.srUser(self.request.user)


class DetalleArticulo(DetailView):
    model = Articulo

    def dispatch(self, request, *args, **kwargs):
        #Articulo1 = 
        Articulo.objects.filter(slug=self.kwargs['slug']).update(visitas = self.get_object().visitas +1)  # Por id -->(id=self.kwargs['pk'])
        #Articulo1.visitas += 1
        #Articulo1.save(force_update=True, update_fields=['visitas'])
        return super().dispatch(request, *args, **kwargs)

class CrearArticulo(LoginRequiredMixin, CreateView):
    model = Articulo
    fields = ["nombre", "descripcion", "categoria", "tipo", "ciudad", "precio", "visible"]

    def get_context_data(self, **kwargs):
        data = super(CrearArticulo, self).get_context_data(**kwargs)
        if self.request.POST:
            data['file'] = FileInlineFormSet(self.request.POST, self.request.FILES)
        else:
            data['file'] = FileInlineFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        file = context['file']
        with transaction.atomic():
            self.object = form.save()
            self.object.creado = self.request.user
            if file.is_valid():
                file.instance = self.object
        return super(CrearArticulo, self).form_valid(form)


class ActualizarArticulo(LoginRequiredMixin, UpdateView):
    model = Articulo
    fields = ["nombre", "descripcion", "categoria", "tipo", "ciudad", "precio", "visible"]

    def dispatch(self, request, *args, **kwargs):
        Articulo1 = Articulo.objects.get(id=self.kwargs['pk'])
        if Articulo1.creado.id == self.request.user.id:
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect(reverse_lazy('inicio'))

    def get_context_data(self, **kwargs):
        data = super(ActualizarArticulo, self).get_context_data(**kwargs)
        if self.request.POST:
            data['file'] = FileInlineFormSet(self.request.POST, self.request.FILES, instance=self.object)
        else:
            data['file'] = FileInlineFormSet(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        file = context['file']
        with transaction.atomic():
            self.object = form.save()

            if file.is_valid():
                file.instance = self.object
                file.save()
        return super(ActualizarArticulo, self).form_valid(form)


class BorrarArticulo(LoginRequiredMixin, DeleteView):
    model = Articulo

    def dispatch(self, request, *args, **kwargs):
        Articulo1 = Articulo.objects.get(id=self.kwargs['pk'])
        if Articulo1.creado.id == self.request.user.id:
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect(reverse_lazy('inicio'))

    def get_success_url(self):
        return redirect(reverse_lazy('ususarioSr'))