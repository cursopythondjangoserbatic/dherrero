from django.apps import AppConfig


class PaginassitioConfig(AppConfig):
    name = 'paginasSitio'
    verbose_name='Paginas genericas'
