from django.shortcuts import render, get_object_or_404
from .models import Pagina


def pagina (request, page_id):
    page = get_object_or_404(Pagina, id = page_id)
    pag = Pagina.objects.all()
    context = {
        'page': page,
    }
    return render(request, "paginasSitio/generico.html", context)
