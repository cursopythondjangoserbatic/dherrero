from django.contrib import admin
from .models import Pagina


class PaginaAdmin(admin.ModelAdmin):
    readonly_fields = ('fechaC','fechaM')
    list_display = ('orden', 'fechaC')


admin.site.register(Pagina, PaginaAdmin)