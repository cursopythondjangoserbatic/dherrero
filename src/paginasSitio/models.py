from django.db import models
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_save
from django.utils.text import slugify


class Pagina(models.Model):
    titulo = models.CharField(max_length=150, verbose_name='Titulo')
    # contenido = models.TextField(verbose_name='Contenido')
    content = RichTextField(verbose_name='Contenido', null=True, blank=True)
    fechaC = models.DateTimeField(auto_now_add=True, verbose_name="Creado")
    fechaM = models.DateTimeField(auto_now=True, verbose_name="Modificado")
    orden = models.SmallIntegerField(verbose_name='Orden', default=0)
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name = 'Pagina'
        verbose_name_plural = 'paginas'
        ordering = ['orden', 'fechaC']

    def __str__(self):
        return self.titulo


def pagina_pre_save_receptor(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.titulo)


pre_save.connect(pagina_pre_save_receptor, sender=Pagina)
