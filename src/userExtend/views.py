from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from .forms import ProfileForm
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy

from .models import Profile


class ProfileDetailView(DetailView):
    model = Profile
    template_name = 'registration/profile_detail.html'

    def get_object(self):
        return get_object_or_404(Profile, user__username=self.kwargs['username'])


class ActualizarPerfil(LoginRequiredMixin, UpdateView):
    form_class = ProfileForm
    success_url = reverse_lazy('profile')
    template_name = 'registration/profile_form.html'

    def get_object(self):
        profile, created = Profile.objects.get_or_create(user=self.request.user)
        return profile

    def form_valid(self, form):
        context = self.get_context_data()
        print(context['profile'].imagen)
        return super(ActualizarPerfil, self).form_valid(form)