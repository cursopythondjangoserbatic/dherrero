from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

"""
def borrador_img(instance, filename):
    old = Profile.objects.get(pk=instance.pk)
    old.imagen.delete()
    return 'users/' + filename
"""

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to='', null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    link = models.URLField(max_length=200, null=True, blank=True)

    class Meta:
        ordering = ['user__username']
""""""
def profile_post_save_receptor(sender, instance, **kwargs):
    if kwargs.get('created', False):
        Profile.objects.get_or_create(user=instance)
        print("Se acaba de crear un usuario y su perfil enlazado")


post_save.connect(profile_post_save_receptor, sender=User)